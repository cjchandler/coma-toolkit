#!/usr/bin/python

import os
from coma import Experiment
from examplepythonsimulation import ExamplePythonSimulation
import matplotlib.pyplot as plt

class Experiment1(Experiment):
    def __init__(self):
        d,f = os.path.split(__file__)
        Experiment.__init__(self, 1, d)
        self.description = 'Demonstrating using Python scripts to run experiments'

    def run(self):
        self.reset()
        self.start()

        s = ExamplePythonSimulation()
        for n in range(1,6):
            s.N = n
            m = self.new_measurement()
            m.start()

            s.init()
            s.run()

            r = s.average()
            m.end()
            m.save(s)

        self.end()
        self.save()

    def plot(self):
        ns = self.measurements['*/parameters/N']
        rs = self.measurements['*/results/average']

        f = plt.figure()
        p = f.add_subplot(1,1,1)
        p.set_xlabel('$N$')
        p.set_ylabel('Average')
        p.plot(ns, rs, 'o-')
        f.savefig('fig.pdf')

if __name__ == '__main__':
    e = Experiment1()
    e.run()
    e.plot()
