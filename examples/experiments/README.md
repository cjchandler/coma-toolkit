# Example Experiments Directory

This is an example data / experiments directory with three example experiments,
one for the minimal Python (`000001`), one for the minimal C++ (`000002`), and
one for the advanced C++ code example (`000003`).

The advanced C++ example deserves an in-depth explanation (the example is in
`examples/advancedC++`, have a look at the README). The example demonstrates
how you can use Python scripts to do a parameter sweep (running a C++ program),
how to manipulate data in xml files and, finally, how to plot. Experiment 3 in
this directory does essentially the same, but uses a slightly different
approach. We thought it useful to show two different ways to achieve the same
results. So what are the differences? First the advancedC++ example assumes a
workflow where you keep your Python scripts in a central place, possibly
together with your simulation code, separate from the experiments. This makes it
easier to have them version controlled (e.g. with Git) and works well if you
have a lot of very similar experiments. Experiment 3 in this directory, on the
other side, envisions a workflow where you keep the scripts and all other data
(postprocessed xml files, graphs) together with the experiment and raw data. You
would have a separate script for each experiment. You can still put them under
version control, but might have to be a bit more careful, not to include binary
raw data files in your Git repository (unless that's what you want). Second, the
advancedC++ example demonstrates a very direct, by-hand approach, whereas
example 3 in this directory uses the coma Python module. The latter is easier,
if you have a good understanding of the coma Python module. (Admittedly, at the
moment it is rather poorly documented.) The former approach only uses standard
Python modules and might also come in handy when you want to do something that
the coma module does not readily support. In the end, which approach is better
for you might also be a matter of preference.

Have a look at `runexperiment.py` in `000003`, it is fairly well documented.
Here, the different Python scripts in the advanced C++ example are implemented
as different methods of the `Experiment3` class. It doesn't correspond to the
advanced C++ example one-to-one, but it is fairly close. The methods demonstrate
how to do a parameter sweep using an external C++ program (`handleData`), how to
plot data, how to modify existing measurements, how to do some postprocessing
and write the output to a new xml file, and, lastly, how to create new
measurements by hand.

Here's a step-by-step guide how the experiments in this directory were created:

    # Install coma.
    coma-toolkit$ ./setup.py install --user
    # Install examplepythonsimulation.
    coma-toolkit$ cd examples/minimalPython
    coma-toolkit/examples/minimalPython$ ./setup.py install --user
    
    # Python example
    coma-toolkit/examples$ coma -d ./experiments create -t python
    coma-toolkit/examples$ cd experiments/000001/
    coma-toolkit/examples/experiments/000001$ chmod a+x runexperiment.py
    coma-toolkit/examples/experiments/000001$ vim runexperiment.py
    coma-toolkit/examples/experiments/000001$ ./runexperiment.py
    
    # C++ example
    coma-toolkit/examples$ cd minimalC++/
    coma-toolkit/examples/minimalC++$ mkdir build
    coma-toolkit/examples/minimalC++$ cd build/
    coma-toolkit/examples/minimalC++/build$ cmake \
      -DDATA_DIRECTORY=\"...coma-toolkit/examples/experiments\" \
      -DEXPERIMENT_DIRECTORY=\"000002\" ..
    coma-toolkit/examples/minimalC++/build$ make
    coma-toolkit/examples/minimalC++/build$ cd ../..
    coma-toolkit/examples$ coma -d ./experiments create
    coma-toolkit/examples$ ./minimalC++/build/handleData

    # Advanced C++ example
    coma-toolkit/examples$ cd advancedC++/
    coma-toolkit/examples/advancedC++$ mkdir build
    coma-toolkit/examples/advancedC++/build$ cmake ..
    coma-toolkit/examples/advancedC++/build$ make
    coma-toolkit/examples/advancedC++/build$ make install
    coma-toolkit/examples/advancedC++/build$ cd ../..
    coma-toolkit/examples$ coma -d ./experiments create -t python -m "Example \
      experiment: Running a C++ program from Python. Analyzing data in Python."
    coma-toolkit/examples$ vim experiments/000003/runexperiment.py
    coma-toolkit/examples$ python experiments/000003/runexperiment.py
    # Make sure you have `~/bin` in your $PATH, otherwise the Python script
    # won't work
