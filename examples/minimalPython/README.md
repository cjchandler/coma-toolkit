# Example Python Simulation

A minimal self-contained example of a computational condensed matter physics
simulation written in Python.

The example demonstrates:

* conventional interface for a Simulation class
* how to organize files
* how to build and install a Python module
* getting a program version number from the Git repository
* unit testing
* TODO: demonstrate serialization

## Installation

To install the Python module `examplepythonsimulation` into the user's home
directory:

    $ ./setup.py install --user

## Running a simple example

    $ python simple_example.py 
    Program: ExamplePythonSimulation
    Version: 0b7b645
    Running...
    Result: 9.5

## Running Unit Tests

    $ python -m unittest discover 

    Result: 9.5

    .
    ----------------------------------------------------------------------
    Ran 1 test in 0.000s

    OK
