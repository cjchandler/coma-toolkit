import numpy as np
from collections import OrderedDict
from . import __version__

class Matrix(object):
    def __init__(self):
        self.N_row = 3
        self.N_col = 5

    def __getstate__(self):
        return OrderedDict([('N_row', self.N_row),('N_col', self.N_col)])

class ExamplePythonSimulation(object):
    def __init__(self):
        self.program = 'ExamplePythonSimulation'
        self.version = __version__
        self.N = 10
        self.m = Matrix()
        self.v = None
        self.average_v = None

    def init(self):
        self.average_v = None
        self.v = np.zeros((self.N,))

    def run(self):
        for i in range(self.N):
            self.v[i] = i

    def average(self):
        self.average_v = self.v.mean()
        return self.average_v

    def __getstate__(self):
        i = OrderedDict([
            ('parameters', OrderedDict([
                        ('N', self.N),
                        ('m', self.m)])),
            ('results', OrderedDict())
            ])
        if self.average_v is not None:
            i['results']['average'] = self.average_v
        return i
