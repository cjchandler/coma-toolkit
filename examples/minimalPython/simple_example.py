from examplepythonsimulation import ExamplePythonSimulation

if __name__ == '__main__':
    s = ExamplePythonSimulation()

    # print information about the simulation
    print('Program: {}'.format(s.program))
    print('Version: {}'.format(s.version))

    # set parameters
    s.N = 20

    # init and run
    print('Running...')
    s.init()
    s.run()

    # retrieve results
    a = s.average()
    print('Result: {}'.format(a))
