import unittest
from examplepythonsimulation import ExamplePythonSimulation

class TestExamplePythonSimulation(unittest.TestCase):
    def test_run(self):
        s = ExamplePythonSimulation()

        # set parameters
        s.N = 20

        # init and run
        s.init()
        s.run()

        # retrieve results
        a = s.average()
        print('\nResult: {}\n'.format(a))
        self.assertEqual(a, 9.5)
