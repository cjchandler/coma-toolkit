import subprocess
import optparse
import re

################################################################################
###ATTENTION!!! this script saves data in the directorys specified below, not ones that were used in cmake
###############################################################################

#making a string that when executed in the shell would run my program with parameters.
nSites = 5
omegaKnot =0.1
nP = 35
lambda_blf = 0.1
description = "\"example sweep in lambda, dummy E \""
executable = "/home/cjchandler/COMA/coma-toolkit/examples/advancedC++/build/handleData" #note you'll have to modify the path here to reflect your system.
data_directory =  "/home/cjchandler/experiments"
experiment = "000002"


#doing a sweep over lambda:
nDataPoints = 10
lambda_blf_start = 0.0
step_size = 0.05

for n in xrange(1, nDataPoints):
    lambda_blf = lambda_blf_start + (n-1)*step_size

    command = executable + " "+ str(nSites) +" " +str(omegaKnot) +" " + str(nP)+ " " +str(lambda_blf)+ " "+description + " " + data_directory + " " + experiment
    p = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE) #It's like command is typed into the terminal, so you can use this for all kinds of stuff pwd etc
    out = p.stdout.read().strip() #out is a python string that stores all the stuff that is printed to screen (c++ std::cout) while the program runs.


    #This isn't nessisary for the example, but it's kind of neat. You can get a few results, parameters, etc into python without loading the xml file the c++ program made by printing to screend
    print(out)  #now find energy. I was careful to cout the keyword and enrergy somewhere in the c++ ouput like this: pythonEnergy -2.3454...
    keyword = 'pythonEnergy'
    befor_keyowrd, keyword, after_keyword = out.partition(keyword)
    E = after_keyword.split()[0]
    E = float(E)
    print '------> this is output from pythonscript, E = ' , E
