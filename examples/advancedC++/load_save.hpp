// this is a header with the actual saving and loading functions in it. And all
// the includes you'll need specific to those
#include <fstream>
#include <vector>
#include <time.h>
#include <string>

#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/serialization/vector.hpp>


#include <boost/serialization/nvp.hpp>
#include <boost/archive/xml_oarchive.hpp>
#include <boost/archive/xml_iarchive.hpp>

#ifndef DATA_DIRECTORY
#define DATA_DIRECTORY "./data"
#endif






#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/foreach.hpp>
#include <string>
#include <set>
#include <exception>
#include <iostream>

#include <stdio.h>


std::string data_directory_global = DATA_DIRECTORY;
std::string experiment_directory_global = EXPERIMENT_DIRECTORY;



struct experimentDindex ///this is the experiment.index xml file in xml format for now.
{
    int last_id;
    void load(const std::string &filename);
    void save(const std::string &filename);
};

void experimentDindex::save(const std::string &filename)
{
    // Create empty property tree object
    using boost::property_tree::ptree;
    ptree pt;

    pt.put("experiments.last_experiment_id", last_id);


    write_xml(filename, pt);

}

void experimentDindex::load(const std::string &filename)
{
    // Create empty property tree object
    using boost::property_tree::ptree;
    ptree pt;

    read_xml(filename, pt);


    last_id = pt.get("experiments.last_experiment_id", 0);


}

struct measurementDindex ///this is the measurement.index xml file in xml format for now.
{
    int last_id;
    void load(const std::string &filename);
    void save(const std::string &filename);
};

void measurementDindex::save(const std::string &filename)
{
    // Create empty property tree object
    using boost::property_tree::ptree;
    ptree pt;

    pt.put("measurements.last_measurement_id", last_id);


    write_xml(filename, pt);

}

void measurementDindex::load(const std::string &filename)
{
    // Create empty property tree object
    using boost::property_tree::ptree;
    ptree pt;

    read_xml(filename, pt);


    last_id = pt.get("measurements.last_measurement_id", 0);


}







int getIndex( void ){
    int out;
    std::string directory = data_directory_global;
    std::string directory2 = experiment_directory_global;

///dummy set up. this will be done with burkhard's command line program eventually:
    bool setup = false;
    if( setup == true){
        experimentDindex expD;
        expD.last_id = 1;
        expD.save( directory+"/experiments.index.xml");
    }



    ///the first thing to do is check that if you've specified say 000004 as your EXPERIMENT_DIRECTORY that this directory exsists. so:
     experimentDindex expD;
     expD.load( directory+"/experiments.index.xml");
     int value = atoi(directory2.c_str());
     if( value > expD.last_id ){ std::cout <<" no experiment directory "<< directory2 <<" found. exiting..."<<std::endl; exit(1);}

    /// that's good. I'm not going to figure out how to make the directory if it's not already there. too hard.
    ///now I'm going to see if there is a measurement.index xml file in the directory that tells me what the last_id is. if not, I'll make one with last_id =0

    measurementDindex expM;
    try{
        expM.load( directory+ "/" + directory2 +"/measurements.index.xml");
    }
    catch(...){ ///this is only activated if there is no measurement.index file there, so make one with index=0 and continue
        expM.last_id = 0;
        //expM.save(directory+ "/" + directory2 +"/experiment.index");
    }
    out = expM.last_id+1;
    ///that's good, now I'll up the index and save it for the next time around
    expM.last_id++;
    expM.save(directory+ "/" + directory2 +"/measurements.index.xml");

    ///return number for filenames
    return out;
};

template <class T>
void saveBin( T &P , std::string &filename){


    //std::stringstream s;
    //s <<  seconds << "_" <<  good_seed()<< ".txt";
    std::stringstream buffer;
    buffer << std::setfill('0') << std::setw(6) << getIndex();//
    filename = buffer.str();

    filename = data_directory_global + "/" + experiment_directory_global + "/"+ filename+ ".bin";

    std::ofstream ofs( filename );

    // save data to archive
    {
        boost::archive::binary_oarchive oa(ofs );
        // write class instance to archive
        oa << P;
    	// archive and stream closed when destructors are called
    }
    /// so the data is saved in a binary format, not human readable. We'll save a
    /// readme with relavent information:
};

template <class T>
void loadBin(T &P , std::string &filename){
     std::ifstream ifs( filename );

    // save data to archive
    {
        boost::archive::binary_iarchive bin(ifs );
        // write class instance to archive

        bin >>  P;
    	// archive and stream closed when destructors are called
    }
};



template<class T>
void saveXml( T &P , std::string &filename){


    std::stringstream buffer;
    buffer << std::setfill('0') << std::setw(6) << getIndex();//
    filename = buffer.str();


    filename = data_directory_global + "/" + experiment_directory_global + "/"+ filename+ ".xml";


    std::ofstream ofs( filename );

    // save data to archive
    {
        boost::archive::xml_oarchive xml(ofs,boost::archive::no_header );
        // write class instance to archive

        xml << boost::serialization::make_nvp( "measurement", P);
    	// archive and stream closed when destructors are called
    }
};

template <class T>
void loadXml( T &P , std::string &filename){///note filename includes the directory
    std::ifstream ifs( filename );

    // save data to archive
    {
        boost::archive::xml_iarchive xml(ifs,boost::archive::no_header );
        // write class instance to archive

        xml >> boost::serialization::make_nvp("FileTitle", P); ///file title is a place holder. It will get overwritten when/if you save again.
    	// archive and stream closed when destructors are called
    }
};

template <class P , class D> ///the idea is P for parameters in xml and D for data in binary
void saveParametersAndData( P &Pstruct , D &Dstruct , std::string &filename){

    std::stringstream buffer;
    buffer << std::setfill('0') << std::setw(6) << getIndex();//
    filename = buffer.str();


    filename = data_directory_global + "/" + experiment_directory_global + "/"+ filename;

    std::ofstream ofs( filename+".xml" );

    // save data to archive
    {
        boost::archive::xml_oarchive xml(ofs,boost::archive::no_header);
        // write class instance to archive

        xml << boost::serialization::make_nvp( "measurement", Pstruct);
    	// archive and stream closed when destructors are called
    }

    std::ofstream ofs2( filename+".bin" );

    // save data to archive
    {
        boost::archive::binary_oarchive oa(ofs2 );
        // write class instance to archive
        oa << Dstruct;
    	// archive and stream closed when destructors are called
    }
};

template <class P , class D>
void loadParametersAndData( P &Pstruct , D &Dstruct , std::string &filename){

    std::ifstream ifs( filename+".xml" );

    // save data to archive
    {
        boost::archive::xml_iarchive xml(ifs,boost::archive::no_header);
        // write class instance to archive

        xml >> boost::serialization::make_nvp("FileTitle", Pstruct); ///file title is a place holder. It will get overwritten when/if you save again.
    	// archive and stream closed when destructors are called
    }

    std::ifstream ifs2( filename+".bin" );

    // save data to archive
    {
        boost::archive::binary_iarchive bin(ifs2 );
        // write class instance to archive

        bin >>  Dstruct;
    	// archive and stream closed when destructors are called
    }
}




///these allow you to pass the data diectory and experiment directory within the program, and not
///predefined with cmake.

template <class T>
void saveBin( T &P , std::string &filename, std::string &data_directory , std::string &experiment_directory){

    data_directory_global = data_directory; 
    experiment_directory_global = experiment_directory;

    //std::stringstream s;
    //s <<  seconds << "_" <<  good_seed()<< ".txt";
    std::stringstream buffer;
    buffer << std::setfill('0') << std::setw(6) << getIndex();//
    filename = buffer.str();

    filename = data_directory + "/" + experiment_directory + "/"+ filename+ ".bin";

    std::ofstream ofs( filename );

    // save data to archive
    {
        boost::archive::binary_oarchive oa(ofs );
        // write class instance to archive
        oa << P;
    	// archive and stream closed when destructors are called
    }
    /// so the data is saved in a binary format, not human readable. We'll save a
    /// xml with relavent information:
};

template<class T>
void saveXml( T &P , std::string &filename, std::string &data_directory , std::string &experiment_directory){

    data_directory_global = data_directory; 
    experiment_directory_global = experiment_directory;

    std::stringstream buffer;
    buffer << std::setfill('0') << std::setw(6) << getIndex();//
    filename = buffer.str();


    filename = data_directory + "/" + experiment_directory + "/"+ filename;


    std::ofstream ofs( filename+ ".xml" );

    // save data to archive
    {
        boost::archive::xml_oarchive xml(ofs,boost::archive::no_header );
        // write class instance to archive

        xml << boost::serialization::make_nvp( "measurement", P);
    	// archive and stream closed when destructors are called
    }
};




template <class P , class D> ///the idea is P for parameters in xml and D for data in binary
void saveParametersAndData( P &Pstruct , D &Dstruct , std::string &filename , std::string &data_directory , std::string &experiment_directory){

    data_directory_global = data_directory; 
    experiment_directory_global = experiment_directory;

    std::stringstream buffer;
    buffer << std::setfill('0') << std::setw(6) << getIndex();//
    filename = buffer.str();

    filename = data_directory + "/" + experiment_directory + "/"+ filename;

    std::ofstream ofs( filename+".xml" );

    // save data to archive
    {
        boost::archive::xml_oarchive xml(ofs,boost::archive::no_header);
        // write class instance to archive


        xml << boost::serialization::make_nvp( "measurement", Pstruct);
    	// archive and stream closed when destructors are called
    }

    std::ofstream ofs2( filename+".bin" );

    // save data to archive
    {
        boost::archive::binary_oarchive oa(ofs2 );
        // write class instance to archive
        oa << Dstruct;
    	// archive and stream closed when destructors are called
    }
};

