

# C++ Advanced Examples

This example is more practical and realistic than the minimal C++ and Python
examples. It demonstrates how to run a C++ program from a Python script (doing a
parameter sweep) and then uses a couple of other Python scripts to do some data
wrangling and plotting.

A different approach to achieving the same results as in this example is shown
in `examples/experiments/0000003`. Have a look at the README in
`examples/experiments`, it has more details on the two different approaches.

Getting started: Do the minimal C++ examples first, then look at these here
which are more practical, and can be modified for your research needs.

## Set up

Run coma create again to make a new experiment to look at the examples in:

    $ coma create

Then build the c++ program in the advancedC++

    $ mkdir build
    $ cd build

If you have multiple compilers installed on your computer, use something like

    $ CXX=/opt/local/bin/c++-mp-4.7 cmake -DDATA_DIRECTORY=\"/Users/burkhard/data\" -DEXPERIMENT_DIRECTORY=\"000002\" ..

otherwise just

    $ cmake -DDATA_DIRECTORY=\"/Users/burkhard/data\" -DEXPERIMENT_DIRECTORY=\"000002\" ..

where you should change `DATA_DIRECTORY` and `EXPERIMENT_DIRECTORY` to match
your setup. `DATA_DIRECTORY` is where to store the data files.
`EXPERIMENT_DIRECTORY` is a subdirectory used to group runs of similar
parameters from a program (a "experiment"). Note that escaping the quotation
marks with '\' is important. Alternatively, you can just run

    $ cmake ..

In this case the C++ code and Python scripts handle the paths dynamically (they
are passed as command line arguments to the C++ program). That's actually the
preferred setup, as you don't have to recompile the C++ code for different
experiments.

Finally:

    $ make
    $ make install

The install command is optional and will install the C++ program `handleData` to
`~/bin` (i.e. `bin` in your home directory). Include this directory in your path
(e.g. put a line `export PATH=~/bin:$PATH` in your `~/.bashrc`) and you'll be
able to run the program just by typing `handleData` (i.e. you don't need to
remember the full path to where `handleData` is installed).

## Running A Parameter Sweep

The C++ program here takes some arguments. You can run it once with:

    $ handleData 15 0.5 10 0.1 "testrun, nonsense data"

If you used the cmake without path you'll need:

    $ handleData 5 0.5 10 0.1 "testrun, nonsense data" "/home/cchand/experiments" "000004"

Again, modify the paths to match your setup.

But what you really want to do in practice is run this program a whole bunch of
times each with slightly different input parameters. A python script is included
for this. To run open `parameter_sweep.py` in a text editor. Then modify the
path 'executable' to be correct fo your computer. If you installed COMA-toolkit
in the home directory on Ubuntu it should be similar to the default. Once that's
done you can run it (in the advancedC++ directory) with:

    $ python parameter_sweep.py

If you check in you experiment directory there should now be a whole lot of
measurement (numerically named) files. Feel free to modify this script for your
research and experiment with it.

## Plotting Data

The next thing you want to do is probably plot your data. Python can do this
too, and a script is provided. Loading all the xml files you want is a little
ticky so the script walks you through it with comments. Run with:

    $ python plot_data.py

Make sure you've run the parameter sweep first so there is data to plot,
otherwise it will give error messages. 
You may also have to instal python plotting packages. On Ubuntu use: sudo apt-get install python-matplotlib

## Modifying Data

There may arise a situation where you want to load data from a C++ generated
xml, perform some calculations in python and then save those calculations again.
It's possible to directly modify the C++ generated xml files, but this is not
good practice since you could accidently overwrite data you needed. Best to make
a new set of xml files. This script `make_data.py` is an example of how to do
so.

    $ python make_data.py

>>>>>>> afb757c5983155c9ebdfdfbf3201c4d3f156e250
