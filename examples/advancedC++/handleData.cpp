#include <iostream>

#include <sstream>
#include <vector>
#include <fstream>
#include <stdlib.h>
#include <time.h>
#include <stdio.h>
#include <map>

#include "load_save.hpp"

#include "boost/date_time/gregorian/gregorian.hpp"
#include "boost/date_time/posix_time/posix_time.hpp"

#include "version.hpp"

///these next three classes get put into the 000001.xml file to list parameters used in simulation
///and some outputs if there aren't too many. Big outputs should go in the 000001.bin file
///the names are standard but there is no reason why you couldn't add more if you have other
/// kinds of data you want to save
class info
{
private:
    friend class boost::serialization::access;

    template<class Archive>
    void serialize(Archive & ar, const unsigned int version)
    {
        using boost::serialization::make_nvp;
        ar & make_nvp("date", Date);
        ar & make_nvp("version", GitVersion);
        ar & make_nvp("description", Description);
    }
public:
    std::string Date;
    std::string GitVersion;
    std::string Description;


    info(){};
    info( std::string des ){
        GitVersion = PROGRAM_VERSION;///this is handled with cmake file and the version.hpp files, and a bash script
        GitVersion = PROGRAM_VERSION;///this is handled with cmake file and the version.hpp files, and a bash script
        ///yes, it's complex but you shouldn't ever have to deal with it, and it's nice to have automatic git version
        ///in the data
        Description = des;
        using namespace boost::posix_time;
        ptime now = microsec_clock::local_time();
        std::stringstream buffer;
        buffer << now;//
        Date = buffer.str();
    };
};

class parameters ///physical parameters for you program. This is an example of mine from an exact diagonlization program.
{
private:
    friend class boost::serialization::access;
    // When the class Archive corresponds to an output archive, the
    // & operator is defined similar to <<.  Likewise, when the class Archive
    // is a type of input archive the & operator is defined similar to >>.
    template<class Archive>
    void serialize(Archive & ar, const unsigned int version)
    {
        using boost::serialization::make_nvp;
        ar & make_nvp("nSites", nSites);
        ar & make_nvp("nPhonons", nP);
        ar & make_nvp("lambda", lambda);
        ar & make_nvp("omegaKnot", omegaKnot);
    }
public:
    int nSites;
    int nP; ///number of phonons
    double lambda; ///electron phonon coupling
    double omegaKnot; ///frequency scale


    parameters(){};
    parameters(int ns, int np, double l, double w   ):
        nSites(ns), nP(np), lambda(l), omegaKnot(w)
    {}
};

class results
{
private:
    friend class boost::serialization::access;
    // When the class Archive corresponds to an output archive, the
    // & operator is defined similar to <<.  Likewise, when the class Archive
    // is a type of input archive the & operator is defined similar to >>.
    template<class Archive>
    void serialize(Archive & ar, const unsigned int version)
    {
        using boost::serialization::make_nvp;
        ar & make_nvp("Energy", E);
        ar & make_nvp("QuasiParticleResidue", Zo);
        ar & make_nvp("XExpectationValues", X);
    }
public:
    double E;
    double Zo;
    std::vector<double> X;

    results(){};
    results(double e, double z, std::vector<double> x ):
        E(e), Zo(z) , X(x)
    {}
};


class filepackage{ ///this puts the three standard headings (and more if you add them yourself) into a class that is saved
    ///with the boost serialization xml version.
    friend class boost::serialization::access;

    template<class Archive>
    void serialize(Archive & ar, const unsigned int version)
    {
        using boost::serialization::make_nvp;
        ar & make_nvp("info", I);
        ar & make_nvp("parameters", P);
        ar & make_nvp("results", R);
    }
public:
    results R;
    parameters P;
    info I;

    filepackage(){};
    filepackage(info i, parameters p, results r ):
        I(i), P(p) , R(r)
    {}
};


class eigenVector ///this is an example class for if you want to save a big output from your program. Here the lowest energy
    ///eigenvector is saved
{
private:
    friend class boost::serialization::access;

    std::vector<double> vectorT;

    template<class Archive>
    void serialize(Archive & ar, const unsigned int version)
    {
        ar & vectorT;
        ar & nStates;
    }

public:
    int nStates;

    eigenVector(){};
    eigenVector( std::vector<double> v , int n ): ///yes you do need to have a constructor.
        vectorT(v) , nStates(n)
    {}
};

int main( int argc, char* argv[] ){///input parametrs from command line, or more likely a script. This avoids all kinds of
    ///recompiling in which you could mess something up and break something in the part that actually does calculations

    ///order:  nSites OmegaKnot MaxPhonons lambda description(string)
    /// example: ./handleData 5 0.5 10 0.1 "testrun, nonsense data"


    int Nsites;
    double OmegaKnot;
    int MaxPhonons;
    double lambda ;

    std::stringstream NsitesString(argv[1]);
    NsitesString >> Nsites;
    std::stringstream omegaKnotString(argv[2]);
    omegaKnotString >> OmegaKnot;
    std::stringstream nPString(argv[3]);
    nPString >> MaxPhonons;
    std::stringstream lambdaString(argv[4]);
    lambdaString >> lambda;
    std::stringstream description(argv[5]);

    std::stringstream data_directoryT(argv[6]);
    std::stringstream experiment_directoryT(argv[7]);
    std::string data_directory = data_directoryT.str();
    std::string experiment_directory = experiment_directoryT.str();

    std::cout << " hi! " <<std::endl;

    std::vector<double> null(Nsites,0.0);

    info infoT(  description.str()  );
    parameters paramT( Nsites, MaxPhonons , lambda , OmegaKnot );

    double Energy = -2.0 + -2.0*lambda*OmegaKnot; ///this is a dummy assignment of energy, just so we can have something to plot later.

    std::cout << " Got an Energy value. pythonEnergy "<<Energy  <<std::endl;

    results resultT( Energy , 400, null  );//( eneregy , quasiparticleresidue , xexpectation values)
    filepackage F( infoT , paramT , resultT );

    std::vector<double> v(10,0.1);

    eigenVector D( v , 10);
    eigenVector D2( null , 5);

    std::string filename;
    saveParametersAndData( F , D , filename , data_directory , experiment_directory);


    std::cout << " all done! " <<std::endl;
}
