import os
import xml.etree.ElementTree as ET #this is a python data structure that corresponds to an xml file
import numpy as np
import sys

data_directory_global = "/home/cchand/COMA_toolkit/examples/experiments"
experiment_global = "000003"


def withinTol( a , b): #can't really use != with double precission as you always have floating point errors
    if abs(a-b) < 1e-15:
        return True

    return False


omegaKnot = 0.1
nSites = 5
nPhonons = 35

def isDataGood( root ):
    #this looks at an element tree and determinies if it matches what you want. Modify this function
    #as nessisary
    include = True

    for L in root.iter('lambda'):
        tempL = float(L.text)
        if (  (tempL > 10) or (tempL <0.0)  ):
            include = False

    for w in root.iter('omegaKnot'):
        if not withinTol( omegaKnot, float(w.text) ):
            include = False

    for n in root.iter('nSites'):
        if not withinTol( nSites , int(n.text) ):
            include = False

    for nP in root.iter('nPhonons'):
        if not withinTol( nPhonons , int(nP.text)  ):
            include = False

    return include



def getData( folder): #this function opens the experiment diectory you're interessted
                    # in or input "" to look through all experiments

    descriptions = {} #this is the output, a python list


    for r,d,f in os.walk(data_directory_global+"/"+folder): #going though all files in the folder directory
        for files in f:
            if files.endswith(".xml"): #if it's an xml file, then we open it and extract the xml into a elementtree structure
                path = os.path.join(r,files)
                #print path
                tree = ET.parse(path )
                root = tree.getroot()
                #print root.tag
                if ( (root.tag == "measurement") and (isDataGood(root) == True) ):#this is loading only the measurment xml files into the EtreeList, so excluding the experiment.xml etc
                    #print root.tag
                    for des in root.iter('description'):
                        descriptions[path] = des.text

    return descriptions

import pprint
v = getData("")
pp = pprint.PrettyPrinter(indent=4)
pp.pprint(v)

#hard to read, so
