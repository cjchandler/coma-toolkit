import os
import xml.etree.ElementTree as ET #this is a python data structure that corresponds to an xml file
import numpy as np
import sys

data_directory_global = "/home/cchand/COMA_toolkit/examples/experiments/"
experiment_global = "000003"

def getData( folder): #this function opens the experiment diectory you're interessted in 000002 if you've been following the examples in order.

    EtreeList = [] #this is the output, a python list

    for r,d,f in os.walk(data_directory_global+folder): #going though all files in the folder directory
        for files in f:
            if files.endswith(".xml"): #if it's an xml file, then we open it and extract the xml into a elementtree structure
                path = os.path.join(r,files)
                #print path
                tree = ET.parse(path )
                root = tree.getroot()
                #print root.tag
                if root.tag == "measurement":#this is loading only the measurment xml files into the EtreeList, so excluding the experiment.xml etc
                    EtreeList.append( tree)

    return EtreeList

def getDataMultiple( folders ): #this function opens the experiment diectory you're interessted in 000002 if you've been following the examples in order.

    EtreeList = [] #this is the output, a python list

    for a in xrange(0,len(folders) ):
        #print folders[a]
        EtreeList = EtreeList + getData(folders[a])

    return EtreeList



def withinTol( a , b): #can't really use != with double precission as you always have floating point errors
    if abs(a-b) < 1e-15:
        return True

    return False



def getEvsL(treeList, omegaKnot , nSites , nP1):
    Energy = []
    Lambda = []

    for a in xrange(0, len(treeList)): #looping through all the xml files in the directory
        include = True
        root = treeList[a].getroot()


        for E in root.iter('Energy'):
            tempE = float(E.text)

        for L in root.iter('lambda'):
            tempL = float(L.text)
            if tempL > 10:
                include = False

        for w in root.iter('omegaKnot'):
            if not withinTol( omegaKnot, float(w.text) ):
                include = False

        for n in root.iter('nSites'):
            if not withinTol( nSites , int(n.text) ):
                include = False

        for nP in root.iter('nPhonons'):
            if not withinTol( nP1 , int(nP.text)  ):
                include = False

        if include == True:
            Energy.append(tempE)
            Lambda.append(tempL)

    if len(Energy) > 0:
        from operator import itemgetter #sorting. Not strictly nessisary, but easier for plotting if you want the points connected by lines
        Lambda, Energy = (list(x) for x in zip(*sorted(zip(Lambda, Energy),key=itemgetter(0))))

    return Energy, Lambda


treeList =  getData( experiment_global ) #this pulls all the measurment data out of an experiment directory

#to look at the xml data from a specific file, you have a couple options. start with getting root:
root = treeList[0].getroot()

#if you remember how the xml file is setup sequentially ( or you can always open it with firefox and check) you
#can access data with indecies. In this example root is like the <measurement> tag, and root[0] is like <info>
#then root[0][2] is the third ( 0 index start like c++ ) element of <info> or <description>. the .tag will return
#the name of that data element ('description') where as .text will return a string with the data stored there.
print( root[0][2].text ) #should print 'example sweep in lambda, dummy E' from tutorial

#that's a little confusing and time consuming, but you can also search for data if you know the name of the tag.
#I know there is somewhere a field called 'Energy' so I can search for that.
for E in root.iter('Energy'): #this loop finds all the xml tags that say <Energy> in the file, and returns them
    #sequentially. #there is only one, so this just prints the energy.
    print E.tag , E.text


#So now we want to get data out to plot.
#say we want to plot energy vs lambda when omegaKnot = 0.1 and nSites = 7, but we don't care about the number of phonons
#and say we only want lambda less than 0.4. This is complex, but you might want to do something like this in research
#and this shows how to do it.

#make two lists, one of energy, one of lambda
Energy = []
Lambda = []
omegaKnot = 0.1
omegaG = omegaKnot
nSites = 5
nP1 = 35

Energy,Lambda = getEvsL( treeList , omegaG , nSites , nP1)

from operator import itemgetter #sorting. Not strictly nessisary, but easier for plotting if you want the points connected by lines
Lambda, Energy = (list(x) for x in zip(*sorted(zip(Lambda, Energy),key=itemgetter(0))))



import matplotlib.pyplot as plt

fig = plt.figure()
plt.xlim([0,0.4])
plt.ylim([-2.1,-2])
ax1 = fig.add_subplot(111)

ax1.set_title("Plot title...")
ax1.set_xlabel('your x label..')
ax1.set_ylabel('your y label...')

ax1.plot(Lambda,Energy,'+-' , label='the data')

leg = ax1.legend()

plt.savefig('foo.pdf')

plt.show()


#that's all for plotting, but what if you have vectors of data in c++ that you saved, adn now you want to get those out as python lists?
root = treeList["000008.xml"].getroot()
def pythonListFromBoostVectorXML( root , vectorName ): #note root is the top element of python element tree. Can get it from a file using something like: treeList= getData( folder); root = treeList["000001.xml"].getroot()
    for X in root.iter(vectorName):
        xvec = []
        for xi in X.findall('item'):
            xvec.append(float(xi.text) )#if the vector is int or some other numerical form change it here, replacing float()

    return xvec
print pythonListFromBoostVectorXML(root , 'XExpectationValues')

#the inverse function, making a python list into something you can save in an element tree using the boost format is included as well
def boostVectorXMLfromPythonList( pylist , vectorName ):
    vectorElement = ET.Element(vectorName)
    count = ET.Element('count')
    count.text = str( len(pylist) )
    item_version = ET.Element('item_version')
    item_version.text = str(0)
    vectorElement.append(count)
    vectorElement.append(item_version)
    for a in xrange(0,int(count.text)):
        item = ET.Element('item')
        item.text = str(pylist[a])
        vectorElement.append(item)

    return vectorElement




