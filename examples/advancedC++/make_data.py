#first load the files you want to modify, or skip to below where there is an example of making a boost compatible element tree from scratch
import os
import xml.etree.ElementTree as ET #this is a python data structure that corresponds to an xml file
import numpy as np
import sys

data_directory_global = "/home/cjchandler/experiments/"

def getData( folder): #this function opens the experiment diectory you're interessted in 000002 if you've been following the examples in order.

    EtreeList = {} #this is the output, a python dictionary. The dictionary is like an (unordered) vector of pairs. Each pair has the file name , and the elementtree for that file in it

    for r,d,f in os.walk(data_directory_global+folder): #going though all files in the folder directory
        for files in f:
            if files.endswith(".xml"): #if it's an xml file, then we open it and extract the xml into a elementtree structure
                path = os.path.join(r,files)
                #print path
                tree = ET.parse(path )
                root = tree.getroot()
                if root.tag == "measurement":#this is loading only the measurment xml files into the EtreeList, so excluding the experiment.xml etc
                    EtreeList[files]=  tree

    return EtreeList

treeList =  getData( "000002" ) #this pulls all the measurment data out of an experiment directory

for value in treeList.iteritems(): #looping through all the xml files in the directory
    root = treeList[value[0] ].getroot()

    for Q in root.iter('QuasiParticleResidue'):
        Q.text  = str( float(Q.text) + 10.5 )

#so now you have a dictionary of modified elementtrees

def saveElementTree( experiment , treeIn , description):
    path = data_directory_global
    experimentpath = path+experiment+"/measurements.index.xml"
    #file = open( path+experiment+"/experiment.xml" , 'w')
    exptree = ET.parse(experimentpath )
    root = exptree.getroot()
    outfilenumber = int(root[0].text)+1
    print outfilenumber
    root[0].text = str(outfilenumber)

    filename = str(outfilenumber).zfill(6) + ".xml"
    #updating index
    file = open(data_directory_global+experiment+"/measurements.index.xml", 'w')
    exptree.write(file)
    file.close()

    #before we save, need to update date, version, maybe description
    root = treeIn.getroot()
    import subprocess
    import optparse
    import re
    p = subprocess.Popen('git describe --always --dirty | tr -d "\n" ', shell=True, stdout=subprocess.PIPE) #It's like command is typed into the terminal, so you can use this for all kinds of stuff pwd etc
    out = p.stdout.read().strip()
    root[0][1].text = out
    import datetime
    root[0][0].text= str(datetime.datetime.now())
    root[0][2].text= description

    file = open(data_directory_global+experiment+"/"+filename, 'w')
    treeIn.write(file)
    file.close()

#now if you want to save them all:
for value in treeList.iteritems(): #looping through all the xml files in the directory
    saveElementTree( "000002" , treeList[ value[0] ] , "modified quasiparticle for demo")



#That's all there is to it. If you want to make an element tree from scratch here's an example: Note it's very verbose, but to modify this example wouldn't be too bad
#making a useful xml from scratch Using Element Tree.
measurement = ET.Element('measurement')
measurement.attrib['class_id'] = "0"
measurement.attrib['tracking_level'] = "0"
measurement.attrib['version'] = "0"
info = ET.Element('info')
info.attrib['class_id'] = "1"
info.attrib['tracking_level'] = "0"
info.attrib['version'] = "0"
measurement.append(info)

date = ET.Element('date' )
date.text = "april 13 2013"
version =ET.Element( 'version')
version.text = "some version hash thing here"
description=ET.Element( 'description')
description.text = "this is test of making an xml file from scratch in python that should be loadable by the boost library in c++"
info.append(date)
info.append( version)
info.append(  description)

parameters = ET.Element('parameters')
parameters.attrib['class_id'] = "2"
parameters.attrib['tracking_level'] = "0"
parameters.attrib['version'] = "0"
measurement.append(parameters)
nSites = ET.Element('nSites')
nSites.text = str(5)
nPhonons = ET.Element('nPhonons')
nPhonons.text = str(1)
lambdablf = ET.Element('lambda')
lambdablf.text = str(0.1)
omegaKnot = ET.Element('omegaKnot')
omegaKnot.text = str(1)
parameters.append(nSites);
parameters.append(nPhonons);
parameters.append(lambdablf);
parameters.append(omegaKnot);


results = ET.Element('results')
results.attrib['class_id'] = "3"
results.attrib['tracking_level'] = "0"
results.attrib['version'] = "0"
measurement.append(results)
Energy = ET.Element('Energy')
Energy.text = str(-2.1)
QuasiParticleResidue = ET.Element('QuasiParticleResidue')
QuasiParticleResidue.text = str(400)
XExpectationValues= ET.Element('XExpectationValues')
count = ET.Element('count')
count.text = str(5)
item_version = ET.Element('item_version')
item_version.text = str(0)
XExpectationValues.append(count)
XExpectationValues.append(item_version)
for a in xrange(0,int(count.text)):
    item = ET.Element('item')
    item.text = str(0)
    XExpectationValues.append(item)

results.append(Energy)
results.append(QuasiParticleResidue)
#results.append(XExpectationValues)

#ATTENTION in this example make sure nSites == XExpectationValues , count  == len( pylist )
def boostVectorXMLfromPythonList( pylist , vectorName ):
    vectorElement = ET.Element(vectorName)
    count = ET.Element('count')
    count.text = str( len(pylist) )
    item_version = ET.Element('item_version')
    item_version.text = str(0)
    vectorElement.append(count)
    vectorElement.append(item_version)
    for a in xrange(0,int(count.text)):
        item = ET.Element('item')
        item.text = str(pylist[a])
        vectorElement.append(item)

    return vectorElement

def pythonListFromBoostVectorXML( root , vectorName ): #note root is the top element of python element tree. Can get it from a file using something like: treeList= getData( folder); root = treeList["000001.xml"].getroot()
    for X in root.iter(vectorName):
        xvec = []
        for xi in X.findall('item'):
            xvec.append(float(xi.text) )#if the vector is int or some other numerical form change it here, replacing float()

    return xvec


pyList = [ 0.0 , 1.2 , 1.4 , 0.0 , 0.3 ]
results.append( boostVectorXMLfromPythonList(pyList , 'XExpectationValues') )

saveElementTree( "000002" , ET.ElementTree(measurement) , "xml from scratch demo")

