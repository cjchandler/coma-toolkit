# C++ Data System

getting started: First you need to make sure the example works on your system, and have the proper file structure in place. 

##File Structure

The easiest way to set this up is to use the COMA command line tool. Go in the root COMA directory and there is a file setup.py which you can run with: 
    
    $./setup.py install --user

There will be a bunch of stuff returned, but look through it until you see where it has installed a binary executable "coma". On Ubuntu the line you're looking for will look something like: 

    $/home/cchand/.local/bin

If you go there you should see an executable called "coma" and you can test that it works by typing: 

    $./coma -h 

Which gives a semi useful help page. You want to be able to use this anywhere though so do: 

    $pwd

And copy the path. Then open .bashrc and paste (using your own path you got from pwd

    $export PATH=$PATH:/home/cchand/.local/bin

Your .bashrc file should be in the home directory on Ubuntu, and you can see all the hidden files (ones starting with .) with 

    $la

And then open it with 

    $gedit .bashrc

Now close all terminal windows and reopen one to load the changes you've made. Now you can type:

    $coma create 

This will make a directory called "experiments" in your home directory, and will initialize all the index and readme files discussed in the main readme. It will print a path to the numerical folder 000001 where you can store the measurement files from the example. Repeating "coma create" will make a folder 000002 etc. 

##BOOST

To save the c++ files as xml and binary files we used the boost libraries. These care be painful to install, but on Ubuntu it should be doable with apt-get use: 
    $sudo apt-get install libboost1.48-dev

These then should be installed in a place where the cmake compiler tool can find them and you don't need to worry about it again. 

## Building

You downloaded the COMA package using git clone https://bitbucket.org/cjchandler/coma-toolkit.git inside a directory, where you ran the setup.py command. Now go from there to /examples/minimalC++

Here's how to build the example with cmake:

    $ mkdir build
    $ cd build

if you have multiple compilers installed on your computer, use:

    $ CXX=/opt/local/bin/c++-mp-4.7 cmake -DDATA_DIRECTORY=\"/Users/burkhard/data\" -DEXPERIMENT_DIRECTORY=\"000001\" ..

else, replacing the path to /experiments\ with your own system specific path, use to make the example output in the 000001 folder :

    $ cmake -DDATA_DIRECTORY=\"/home/cchand/experiments\" -DEXPERIMENT_DIRECTORY=\"000001\" ..

then finally:

    $ make

`DATA_DIRECTORY` is where to store the data files. `EXPERIMENT_DIRECTORY` is a subdirectory used to group runs of similar parameters from a program. Note that escaping the quotation marks with \ are important.

I ran into issues on OS X with MacPorts (runtime errors like `pointer being
freed was not allocated`): Make sure that Boost and this code are compiled with
the same compiler and same version of the compiler.
There may be warning messages from the compiler, but on ubuntu it should still create the executable handleData file.

## Running

Make sure the data directory, and project directory actually exist, they should have been created by the coma create command. There's no proper error handling in case they do not, but if you've followed this readme it should work fine. To test run the executable

    $./handleData
