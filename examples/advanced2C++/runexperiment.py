#!/opt/local/Library/Frameworks/Python.framework/Versions/2.7/Resources/Python.app/Contents/MacOS/Python

import os
from coma import Experiment, XMLArchive
from collections import OrderedDict
import subprocess
import matplotlib.pyplot as plt

class Experiment3(Experiment):
    def __init__(self):
        d,f = os.path.split(__file__)
        Experiment.__init__(self, 3, d)
        self.description = 'Example experiment: Running a C++ program from ' + \
                           'Python. Analyzing data in Python.'
        # Assumes that handleData is installed somewhere in the Bash $PATH
        self.executable = 'handleData'
	
    def run(self):
        # reset() resets the measurements directory. Hence, existing
        # measurements (e.g. 000001.xml, 000002.xml, and so on) are
        # overwritten.
        #
        # start() and end() are convenience methods recording the start and end
        # date and time of the experiment. Those are usually written to
        # `experiment.readme.xml`
        # 
        # save() stores the experiment, i.e. all metadata like id, description,
        # start and end dates, to `experiment.readme.xml`.
        self.reset()
        self.start()
        
        self.parameter_sweep()
        
        self.end()
        self.save()

    def parameter_sweep(self):
        # As a demonstration, this experiment does a parameter sweep for our
        # simulation, which is an external C++ program `handleData` from the
        # directory `examples/advancedC++` (compiled and installed separately).
        # `handleData` expects a couple of command line arguments in a specific
        # order. Have a look at the code of `handleData` for more details.
        # 
        # `handleData` creates the necessary measurement files (e.g.
        # `000001.xml`, and so on) by itself and writes data to them.
        nSites = 5
        omegaKnot = 0.1
        nP = 35
        lambda_blf = 0.1
        description = "example sweep in lambda, dummy E"
        # The C++ program expects the experiments / data directory and the
        # directory of the experiment itself as separate arguments. Hence, we
        # split self.dir into those two parts.
        experimentDirectory,experiment = os.path.split(self.dir)

        # doing a sweep over lambda:
        nDataPoints = 10
        lambda_blf_start = 0.0
        step_size = 0.05
        for n in xrange(nDataPoints):
            lambda_blf = lambda_blf_start + n*step_size
            args = [self.executable, str(nSites), str(omegaKnot), str(nP),
                    str(lambda_blf), description, experimentDirectory,
                    experiment]
            # This executes the program (`handleData` in self.executable) with
            # the above arguments. The programs output goes to the variable
            # `out` as a string.
            out = subprocess.check_output(args)
            print(out)

            # This isn't necessary for the example, but it's kind of neat. You
            # can get a few results, parameters, etc into python without
            # loading the xml file by looking through the c++ program output
            # (printed to the screen).
            #
            # Now find the energy. I was careful to cout the keyword and
            # energy somewhere in the c++ output like this: 
            # pythonEnergy -2.3454...
            keyword = 'pythonEnergy'
            befor_keyowrd, keyword, after_keyword = out.partition(keyword)
            E = after_keyword.split()[0]
            E = float(E)
            print('------> this is output from pythonscript, E = {}'.format(E))

    def plot(self):
        # For floating point comparisons
        def epsilonEqual(a,b): return abs(a-b)<1e-15

        # First, we show how we can retrieve data from the measurements. As an
        # example, we get the description and energy of the eighth measurement.
        #
        # To explore what parameters, results and info fields are available (in
        # case you don't remember all of them), the `coma` command line utility
        # comes in handy. For example, 
        #   $ coma retrieve '/*'
        # shows all available experiments, 
        #   $ coma retrieve '/3/measurements/*'
        # shows all measurements of experiment 3, and
        #   $ coma retrieve '/3/measurements/*/results'
        # shows the results of all measurements of experiment 3.
        d = self.measurements[8]['info']['description']
        E = self.measurements[8]['results/Energy']
        # The above line is equivalent to:
        # E = self.measurements[8]['results']['Energy']
        print('Measurement 8')
        print('  Description: {}'.format(d))
        print('  Energy: {}'.format(E))
        print('')

        # Now let us do something more practical. We retrieve a list of all
        # nSites, lambdas, omegas and energies, filter them by some criteria
        # and then plot energy over lambda. The resulting graph is saved to
        # `foo.pdf`.
        ns = self.measurements['*/parameters/nSites']
        ls = self.measurements['*/parameters/lambda']
        ws = self.measurements['*/parameters/omegaKnot']
        Es = self.measurements['*/results/Energy']
        # Just as a safety, make sure all list have the same number of entries
        assert(len(ns) == len(ls) == len(ws) == len(Es))

        # Here we filter the lists. The final lambdas and energies for plotting
        # are stored in plot_ls ad plot_Es.
        plot_ls = []
        plot_Es = []
        for i,_ in enumerate(ns):
            if ls[i] > 0.4:
                continue
            if not epsilonEqual(ws[i], 0.1):
                continue
            if ns[i] != 5:
                continue
            plot_ls.append(ls[i])
            plot_Es.append(Es[i])
        
        # Plot with the awesome matplotlib
        fig = plt.figure()
        plt.xlim([0,0.4])
        plt.ylim([-2.1,-2])
        ax1 = fig.add_subplot(111)

        ax1.set_title("Plot title...")
        ax1.set_xlabel('your x label..')
        ax1.set_ylabel('your y label...')
        
        # This creates the actual plot
        ax1.plot(plot_ls, plot_Es, label='the data')
        leg = ax1.legend()
        plt.savefig(os.path.join(self.dir, 'foo.pdf'))

    def modify_measurements(self):
        # If for some reason you want to modify existing measurements, that's
        # how you can do it. We increase the value of QuasiParticleResidue by
        # 10.5 for all measurements.
        for m in self.measurements:
            m.load()
            d = m.data
            if d['results'].has_key('QuasiParticleResidue'):
                d['results']['QuasiParticleResidue'] += 10.5
                m.save(d)
        # TODO: Measurement class should make it easier to modify existing,
        #       saved measurements

    def postprocess(self):
        # Let's assume we want to do some postprocessing and write the results
        # to a different xml file. This demonstrates how you retrieve data from
        # measurements and how to write (and read) xml files using coma's
        # XMLArchive class.
        #
        # First, retrieve the energy from all measurements and calculate it's
        # average. We also retrieve the list of all measurement ids so that
        # later we know which measurements were used to calculate the average.
        ids = [m.id for m in self.measurements]
        Es = self.measurements['*/results/Energy']
        E_avg = float(sum(Es)) / len(Es)

        # Second, we construct a nested `OrderedDict` representing the
        # information that we want to store to the xml file. We could as well
        # use the normal Python `dict` (`{}`), but it does not keep the order
        # of the entries. Personally, I find having things in some specific
        # order increases readability (if you every have to look at the xml
        # files directly, by hand), otherwise the order does not matter.
        d = OrderedDict()
        d['info'] = OrderedDict()
        d['info']['program'] = 'runexperiment.py'
        d['info']['description'] = 'postprocessing data'
        d['info']['experiment'] = self.id
        d['info']['processed_measurements'] = ids
        d['results'] = OrderedDict()
        d['results']['E_average'] = E_avg

        # Construct an XMLArchive. The argument specifies the xml document tag,
        # e.g. in this case the whole document will be enclosed in a
        # <processed_data>...</processed_data> tag. We write the data to the
        # file `processed.xml`.
        a = XMLArchive('processed_data')
        filename = os.path.join(self.dir, 'processed.xml')
        f = open(filename, 'w')
        a.dump(d, f)
        f.close()

        # Just as a brief demonstration, we can also read xml files. In the
        # simplest case they are returned, surprise, as a nested OrderedDict.
        # Note that in this example we transparently stored a Python list (ids
        # / processed_measurements is a list) to xml and also read it back.
        f = open(filename)
        d2 = a.load(f)
        f.close()
        assert(d == d2)
        assert(d2['info']['processed_measurements'] == ids)

    def custom_measurement(self):
        # Let's say we want to create a new measurement with custom data from
        # scratch. We build a nested OrderedDict holding all the information.
        d = OrderedDict()
        d['info'] = OrderedDict()
        d['info']['program'] = 'runexperiment.py'
        d['info']['description'] = 'a custom measurement'
        d['parameters'] = OrderedDict()
        d['parameters']['a'] = 1
        d['parameters']['b'] = 2
        d['results'] = OrderedDict()
        d['results']['a_plus_b'] = 3
        d['results']['a_times_b'] = 2

        # Now we create a new measurement and save the above OrderedDict to
        # xml. The measurement id, filename and directory, and so on are
        # automatically handled.
        m = self.new_measurement()
        m.save(d)

if __name__ == '__main__':
    e = Experiment3()
    e.run()
    e.plot()
    e.modify_measurements()
    e.postprocess()
    e.custom_measurement()
