#Advanced2C++ example

This example shows how to use python structures and classes to accomplish 
the same tasks as the python script files in advancedC++ example. There are
also demonstrated other ways of acessing the data via the command line coma
tool. 
The main readme for this is in examples/experiments, but the python script is copied here for continuity. 

##How to Run: 
Follow instructions in examples/experiments, it will not run without modification from here.
