# Example Python C++ Simulation

A self-contained example of a computational condensed matter physics simulation
written in Python and C++.

The example demonstrates:

* conventional interface for a Simulation class
* how to organize files
* how to build and install a Python extension module
* using a Git program version both in C++ and Python
* Python/C++ integration
    * passing std::vectors and Eigen vectors and matrices back to Python as
      Numpy arrays, without copying the data
* TODO: demonstrate serialization
* TODO: printing to stdout from C++ does not seem to work properly on my OS X
  system

## Building and Installation

Requirements:

* cmake
* python
* boost
* Eigen
* Boost.Numpy <https://github.com/ndarray/Boost.NumPy>

To build and install Boost.Numpy:

    $ git clone git://github.com/ndarray/Boost.NumPy.git
    $ cd Boost.NumPy/
    # install scons, if necessary
    $ sudo scons --with-boost=/opt/local/ install

Looks like newer versions of Boost.Numpy also have a cmake build system.

Build and install the examplepythoncppsimulation Python Module into the user's
home directory:

    $ mkdir build
    $ cd build
    $ cmake ..
    # or, to compile with a specific compiler:
    # $ CXX=/opt/local/bin/g++-mp-4.7 cmake ..
    $ make
    $ cd ..
    $ ./setup.py install --user

## Running a simple example

    $ python simple_example.py 
    Program: ExampleSimulation
    Version: 0b7b645-dirty
    Running...
    Results: (2.5, 5.5, 16.5)
    
    ...
