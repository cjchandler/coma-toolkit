import os
import sys
import stat
from string import Template
import argparse
import ConfigParser
from .experimentsdirectory import ExperimentsDirectory
from . import __version__
from . import xdgbasedirectory as xdg
from collections import OrderedDict

_CONFIG_FILE='''\
[coma]
experiments_directory=~/experiments
'''

class CommandLineInterface(object):
    def __init__(self):
        self.program = 'coma'
        self.configfile = 'preferences.conf'
        self.directory = '~/experiments'
        self._load_config()

    def _load_config(self):
        # Create config file if it does not exist
        p = xdg.save_config_path(self.program)
        p = os.path.join(p, self.configfile)
        if not os.path.exists(p):
            try:
                print('Creating config file "{}"'.format(p))
                f = open(p, 'w')
                f.write(_CONFIG_FILE)
                f.close()
            except IOError:
                print('Warning: Could not create config file "{}"'.format(p))
        
        # Read config file
        try:
            ps = xdg.load_config_paths(self.program, self.configfile)
            p = ps.next()
            c = ConfigParser.RawConfigParser()
            c.read(p)
            if c.has_option('coma', 'experiments_directory'):
                self.directory = c.get('coma', 'experiments_directory')
        except StopIteration:
            print('Warning: Could not find config file "{}"'.format(self.configfile))

    def _get_parser(self):
        parser = argparse.ArgumentParser(
                description='Manage computational "experiments".')
        parser.add_argument(
                '-d', '--directory', 
                help='directory where all experiments are stored; default: ~/experiments', 
                default=self.directory)
        parser.add_argument(
                '--version', 
                action='version', 
                version=__version__)
        subparsers = parser.add_subparsers(
                title='Commands', 
                description='Type `coma <command> --help` for more information '
                            'on a specific command.', 
                help='')
        
        # Create command
        create = subparsers.add_parser('create', help='Create a new experiment')
        create.add_argument(
                '-t', '--template',
                help='Template to use when creating a new experiment; '
                     'default: "default"',
                default='default')
        create.add_argument(
                '-m', '--description',
                help='Description of the new experiment. Used, for example, in '
                     'the `experiment.readme.xml` file. Internally sets the '
                     '${description} variable for use in the template files.',
                default='')
        create.set_defaults(function=self.create_experiment)

        retrieve = subparsers.add_parser(
                'retrieve', 
                help='Retrieve data from the experiments')
        retrieve.add_argument(
                'path',
                help="Path describing which data to retrieve. Examples: '/', "
                     "'/*', '/1/info'")
        retrieve.set_defaults(function=self.retrieve)

        reset = subparsers.add_parser('reset', help='Reset an experiment')
        reset.add_argument(
                'experiment_id',
                type=int,
                help='Reset the experiment with this id')
        reset.set_defaults(function=self.reset)

        return parser

    def create_experiment(self, args):
        print('Creating experiment')
        ed = ExperimentsDirectory(self.directory)
        e = ed.new_experiment(from_template=args.template, description=args.description)
        print('Created experiment {} at {}'.format(e.id,e.dir))

    def retrieve(self, args):
        print('Retrieving data at path "{}"\n'.format(args.path))
        ed = ExperimentsDirectory(self.directory)
        ds = ed[args.path]
        if not isinstance(ds,list):
            ds = [ds]
        for d in ds:
            print(self.prettystr(d))

    def prettystr(self, d, indent=0):
        if isinstance(d, OrderedDict):
            s = '\n'
            for k,v in d.iteritems():
                s += '{}{}: {}\n'.format(' '*indent, str(k), self.prettystr(v, indent+2))
            return s[:-1]
        else:
            return str(d)

    def reset(self, args):
        ed = ExperimentsDirectory(self.directory)
        e = ed[args.experiment_id]
        print('I am about to reset the experiment\'s measurement index to zero. \n'
              'That means that on the next run existing data files will very \n'
              'likely be overwritten. \n\nReset experiment {} at {} (yes/[no])?'
              .format(e.id, e.dir))
        r = sys.stdin.readline()
        if r.strip() == 'yes':
            e.reset()
            print('Successfully reset experiment {}'.format(e.id))

    def main(self, argv):
        parser = self._get_parser()
        args = parser.parse_args()
        self.directory = args.directory
        args.function(args)
