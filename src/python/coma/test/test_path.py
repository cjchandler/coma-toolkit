import unittest
from collections import OrderedDict
from coma import access_data_by_path as get

class TestAccessDataByPath(unittest.TestCase):
    class A(object):
        def __iter__(self):
            self.i = -1
            return self

        def next(self):
            self.i += 1
            if self.i>9:
                raise StopIteration
            return self.i

        def __getitem__(self,i):
            if i<0 or i>9:
                raise IndexError()
            return i

    def setUp(self):
        self.a = self.A()
        self.d = OrderedDict([
                ('a', 1),
                ('b', 2),
                ('c', 3),
                ('d', OrderedDict([
                    ('a', 4),
                    ('b', 5),
                    ('c', 6),
                    ('e', None)])),
                ('e', OrderedDict([
                    ('a', 7),
                    ('b', 8),
                    ('c', OrderedDict([('a', 9), ('b', 10)]))])),
                ('f', [
                    [11,12,13,None],
                    OrderedDict([('a', 14), ('b', 15)]),
                    OrderedDict([('a', 16), ('b', 17), ('c', OrderedDict([('a',18)]))]),
                    OrderedDict([('d', 19)]),
                    self.a,
                    None]),
                ('g', [20,21,22,23,24,25]),
                ('h', None),
                ('i', 'text')
                ])

    def test_simple_access(self):
        self.assertEqual(get(self.d,'a'), 1)
        self.assertEqual(get(self.d,'b'), 2)
        self.assertEqual(get(self.d,'c'), 3)
        self.assertEqual(get(self.d,'d/a'), 4)
        self.assertEqual(get(self.d,'d/b'), 5)
        self.assertEqual(get(self.d,'d/c'), 6)
        self.assertEqual(get(self.d,'d/e'), None)
        self.assertEqual(get(self.d,'e/a'), 7)
        self.assertEqual(get(self.d,'e/b'), 8)
        self.assertEqual(get(self.d,'e/c'), OrderedDict([('a', 9), ('b', 10)]))
        self.assertEqual(get(self.d,'e/c/a'), 9)
        self.assertEqual(get(self.d,'e/c/b'), 10)
        self.assertEqual(get(self.d,'g'), [20,21,22,23,24,25])
        self.assertEqual(get(self.d,'h'), None)
        self.assertEqual(get(self.d,'i'), 'text')

        self.assertEqual(get(self.d,''), self.d)
        
        with self.assertRaises(KeyError):
            get(self.d,'e/d')

    def test_array_access(self):
        self.assertEqual(get(self.d,'f/0'), [11,12,13,None])
        self.assertEqual(get(self.d,'g/3'), 23)
        self.assertEqual(get(self.d,'f/0/1'), 12)
        self.assertEqual(get(self.d,'f/3'), OrderedDict([('d', 19)]))
        self.assertEqual(get(self.d,'f/1/b'), 15)
        self.assertEqual(get(self.d,('f',1,'b')), 15)
        self.assertEqual(get(self.d,'f/4'), self.a)
        self.assertEqual(get(self.d,'f/5'), None)

    def test_asterisk_access(self):
        self.assertEqual(get(self.d,'d/*'), [4,5,6,None])
        self.assertEqual(get(self.d,'*/a'), [4,7])
        self.assertEqual(get(self.d,'e/c/*'), [9,10])
        self.assertEqual(get(self.d,'e/*/a'), [9])
        self.assertEqual(get(self.d,'g/*'), [20,21,22,23,24,25])
        self.assertEqual(get(self.d,'f/*/a'), [14,16])
        self.assertEqual(get(self.d,'f/4/*'), [0,1,2,3,4,5,6,7,8,9])
        self.assertEqual(get(self.d,'*/1/a'), [14])
        self.assertEqual(get(self.d,'f/*/5'), [5])

        with self.assertRaises(KeyError):
            get(self.d,'*/d')

    def test_multiple_asterisk_access(self):
        self.assertEqual(get(self.d,'*/*/a'), [9,14,16])
        self.assertEqual(get(self.d,'*/*/d'), [19])
        self.assertEqual(get(self.d,'f/*/*'), [11,12,13,None,14,15,16,17,
                                               OrderedDict([('a',18)]),
                                               19,0,1,2,3,4,5,6,7,8,9])
        self.assertEqual(get(self.d,'*/c/*'), [9,10])
        self.assertEqual(get(self.d,'f/*/*/*'), [18])

        with self.assertRaises(KeyError):
            get(self.d,'*/*/e')
        with self.assertRaises(KeyError):
            get(self.d,'*/d/*')
        with self.assertRaises(KeyError):
            get(self.d,'h/*/*')
