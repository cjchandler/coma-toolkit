import os
import shutil
from .serialization import XMLArchive
from .util import create_lockfile, remove_lockfile, DirectoryError
from .path import access_data_by_path
from .experiment import Experiment

class ExperimentsDirectory(object):
    def __init__ (self, edir = '~/experiments'):
        self.dir = os.path.expanduser(edir)
        self.indexfile = os.path.join(self.dir, 'experiments.index.xml')
        self.indexfile_mtime = None
        self.templatesdir = os.path.join(self.dir, 'templates')
        self.last_experiment_id = 0
        self._iter_i = 1
        if not os.path.exists(self.dir):
            self.create_empty_directory()
        elif not os.path.exists(self.indexfile):
            raise DirectoryError(
                    'Not an experiments directory: "{}" does not contain an '
                    'index file "experiments.index.xml"'.format(self.dir))
        self.read_indexfile()

    def create_empty_directory(self):
        os.mkdir(self.dir)
        self.create_empty_indexfile()
        d,_ = os.path.split(__file__)
        src_dir = os.path.join(d,'data', 'templates')
        dst_dir = self.templatesdir
        shutil.copytree(src_dir, dst_dir)
        # We rely on the existence of a default template, even if it is empty
        default_dir = os.path.join(dst_dir, 'default')
        if not os.path.exists(default_dir):
            os.mkdir(default_dir)

    def create_empty_indexfile(self):
        o = {'last_experiment_id': 0}
        if os.path.exists(self.indexfile):
            raise DirectoryError('Can\'t create index file: File already exists')
        f = open(self.indexfile, 'w')
        a = XMLArchive('experiments')
        a.dump(o,f)
        f.close()

    def path_of_experiment(self, experiment_id):
        return os.path.join(self.dir, '{:06d}'.format(experiment_id))

    def new_experiment(self, from_template='default', **kwargs):
        # Does the template exist?
        t = os.path.join(self.templatesdir, from_template)
        if not os.path.exists(t):
            raise DirectoryError('Could not find template "{}" in directory "{}"'.
                    format(from_template, self.templatesdir))

        # Read and increment id of the last experiment
        create_lockfile(self.indexfile)
        
        f = open(self.indexfile, 'r+')
        a = XMLArchive('experiments')
        o = a.load(f)
        o['last_experiment_id'] += 1
        experiment_id = o['last_experiment_id']
        
        f.truncate(0)
        f.seek(0)
        a.dump(o,f)
        f.close()
        
        remove_lockfile(self.indexfile)

        # Create the experiment
        p = self.path_of_experiment(experiment_id)
        e = Experiment(p, experiment_id, from_template=t, **kwargs)
        self.last_experiment_id = experiment_id
        return e

    def reload(self):
        if self.indexfile_mtime != os.path.getmtime(self.indexfile):
            self.read_indexfile()

    def read_indexfile(self):
        self.indexfile_mtime = os.path.getmtime(self.indexfile)
        f = open(self.indexfile)
        a = XMLArchive('experiments')
        o = a.load(f)
        f.close()
        self.last_experiment_id = o['last_experiment_id']

    def __iter__(self):
        self._iter_i = 0
        return self

    # TODO: For each Experiment, if in the experiment directory there is a
    # subclass of Experiment, e.g. class Experiment5 for experiment 5, then it
    # would be cool to load this subclass instead of the generic class
    # Experiment
    def next(self):
        self._iter_i += 1
        if self._iter_i > self.last_experiment_id:
            raise StopIteration()
        return Experiment(self.path_of_experiment(self._iter_i), self._iter_i)

    def __len__(self):
        return self.last_experiment_id

    def __getitem__(self, i):
        if isinstance(i,int):
            if i<1 or i>self.last_experiment_id:
                raise IndexError()
            return Experiment(self.path_of_experiment(i), i)
        elif isinstance(i,basestring) and (i.find('/') != -1 or i.isdigit() or i == '*' or i == ''):
            return access_data_by_path(self, i)
        else:
            raise KeyError(i)

    def __str__(self):
        s = 'Experiments directory "{}" with {} experiments'.format(
                self.dir, self.last_experiment_id)
        return s
