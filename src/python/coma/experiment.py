from collections import OrderedDict
import os
import sys
import shutil
from string import Template
from .serialization import XMLArchive
from .path import access_data_by_path
from .measurementsdirectory import MeasurementsDirectory
from .util import current_date_as_string, DirectoryError

class ExperimentError(Exception):
    pass

class Experiment(object):
    def __init__(self, experiment_dir, experiment_id=None, description=None, 
                 tags=[], from_template=None):
        self.dir = experiment_dir
        self.id = experiment_id
        self.description = description
        self.tags = tags
        self.template_dir = from_template
        
        self.file = os.path.join(self.dir, 'experiment.readme.xml')
        self.start_date = None
        self.end_date = None
        self.data = {}

        if self.template_dir is not None:
            self._create_from_template()
        
        # note: this will create the directory if it does not exist yet
        self.measurements = MeasurementsDirectory(self.dir)
        if not os.path.exists(self.file):
            self.save()
        else:
            self.load()

    def _create_from_template(self):
        if not os.path.exists(self.template_dir):
            raise DirectoryError('Template directory "{}" does not exist'
                                 .format(self.template_dir))
        if os.path.exists(self.dir):
            raise DirectoryError('Cannot create a new experiment in "{}" from '
                                 'template "{}": The directory already exists'
                                 .format(self.dir, self.template_dir))
        variables = {
            'experiment_id': self.id,
            'description': self.description,
            # TODO: this likely does not work with string.Template
            'tags': self.tags,
            'python': sys.executable
        }
        shutil.copytree(self.template_dir, self.dir)
        files = [os.path.join(r,f) for r,_,fs in os.walk(self.dir) for f in fs 
                 if f.endswith('.template')]
        for f in files:
            self._fill_template(f, variables)
            
    def _fill_template(self, template_file, variables):
        assert(template_file.endswith('.template'))
        dst_file = template_file[:-len('.template')]
        f = open(template_file)
        s = f.read()
        f.close()
        t = Template(s)
        s = t.substitute(variables)
        os.rename(template_file, dst_file)
        f = open(dst_file, 'w')
        f.write(s)
        f.close()

    def save(self):
        i = OrderedDict([
            ('info', OrderedDict([
                        ('experiment_id', self.id),
                        ('description', self.description),
                        ('tags', self.tags),
                        ('start_date', self.start_date),
                        ('end_date', self.end_date)]))
            ])
        backupfile = self.file + '.backup'
        if os.path.exists(self.file):
            os.rename(self.file, backupfile)

        f = open(self.file, 'w')
        a = XMLArchive('experiment')
        a.dump(i, f)
        f.close()
        
        if os.path.exists(backupfile):
            os.remove(backupfile)

        # measurements are not saved to the experiment xml file
        self.data = i
        self.data['measurements'] = self.measurements

    def load(self):
        f = open(self.file)
        a = XMLArchive('experiment')
        self.data = a.load(f)
        f.close()

        i = self.data['info']
        if self.id is not None and str(self.id) != str(i['experiment_id']):
            raise ExperimentError('Trying to load experiment "{}" from file "{}", '
                    'but this is experiment "{}"'
                    .format(i['experiment_id'], self.file, self.id))
        
        self.id = i['experiment_id']
        for k in ['description','start_date','end_date']:
            if i.has_key(k):
                self.__setattr__(k, i[k])
        
        self.data['measurements'] = self.measurements

    def start(self):
        self.start_date = current_date_as_string()

    def end (self):
        self.end_date = current_date_as_string()

    def run(self):
        raise NotImplementedError()

    def reset(self):
        self.measurements.reset()

    def new_measurement(self):
        return self.measurements.new_measurement()

    def __iter__(self):
        return self.data.itervalues()

    def __getitem__(self, i):
        return access_data_by_path(self.data,i)

    def __str__(self):
        s = 'Experiment {}'.format(self.id)
        if self.data.has_key('info'):
            i = self.data['info']
            for k,v in i.iteritems():
                if k != 'experiment_id' and v:
                    s += '\n  {}: {}'.format(k,v)
        s += '\n  Fields: {}'.format(self.data.keys())
        s += '\n  {} measurements\n'.format(len(self.measurements))
        return s
