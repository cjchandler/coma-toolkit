# COMA: Condensed Matter Theory Programming Toolkit

## Objective

> Establish conventions and provide a framework for data management for
> computational condensed matter projects.

The main idea of COMA is to have a standard format for saving the output from
computations and a system of directories so that you don't get lost in the huge
number of files produced. Once your data is saved in the COMA standard, you can
easily modify the python scripts included, or write your own scripts, for
manipulating that data and plotting. C++ and Python are supported for actually
doing calculations and saving/loading data, but most scripts are purely python
since file systems with c++ is pretty messy. 

## Contents

COMA consists of several parts:

1. A command line utility `coma`.  
   Helps with creating directories and files, as well as retrieving and finding
   data.
2. Python module `coma`.  
   A couple of classes to easily store, retrieve and manipulate data.
3. C++ header file. (Currently part of the C++ example project.)  
   Simple and easy to use header to store and restore data.
4. Example projects
    1. A minimal pure C++ example (in `exmamples/minimalC++`)
    2. A minimal pure Python example (in `examples/minimalPython`)
    3. A minimal Python/C++ example (in `examples/minimalPythonC++`)
    4. An example data ("experiments") directory (in `examples/experiments`)

Each of the examples come with their own detailed README files in their
respective directories. At the time of this writing, the pure C++ and Python
examples are up-to-date, while the Python/C++ example is mostly a demonstration
of how to interface C++/Eigen with Numpy.

## Concepts

### Simulation

A simulation class holds a numerical simulation. It can be configured (setting
input parameters), initialized and run, typically in that order. Afterwards
results are retrieved. A simulation should have a (program) name and a version.

### Measurement

A measurement is one run of one specific simulation with one set of input
parameters and hence a unique computational result. In practice a measurement is
likely very similar to the simulation class itself, but possibly with additional
meta data, like start date, end date, and measurement id.

### Experiment

An experiment is a set of related measurements. It is therefore simply a means
of grouping measurements. Typical examples are parameter scans for a particular
simulation, or chaining simulations, where simulation 2 processes the results of
simulation 1. All data related to the experiment is kept together, in one
directory. The directory might contain raw data files, post-processed data,
graphs, and a script to run the experiment. However, most importantly it
contains a file in a standard format, with a conventional data layout and a
common filename that describes the experiment: Input parameters, results, meta
data. While data and workflows will vary widely from project to project, by
following conventions for the data and file layout, it should be easier to
establish best practices and write common data management tools that work across
very different projects.

## Data and file layout specification

The main idea is that wherever you are working on you computer, writing and
testing code (hopefully with version control like git), your data should all be
saved in one central location. This is the `experiments` directory. Each
experiment has an `experiment_id`. In the `experiments` directory, the file
`experiments.index.xml` keeps track of the latest `experiment_id` and the
experiments themselves are stored in sequentially numbered sub-directories, e.g.
`000001` and `000002`---that is, the sub-directories are the `experiment_id`.

Each experiment directory in turn contains the files `experiment.readme.xml`,
`measurements.index.xml`, and many files named `000001.xml`, `000002.xml`, etc,
and conceivably a number of raw data files, like `000001.bin`, `000002.bin`, and
so on. Analogous to the `experiments` directory containing experiments, each
experiment directory itself contains several measurements with
`measurement_id`s. The latest `measurement_id` is recorded in
`measurements.index.xml`. Files corresponding to each measurement are again
named by the `measurement_id` itself. 

The `experiment.readme.xml` file gives some information about the program you
are using, date started, parameters, and any other things you think are
important to remember about the program that produced the data files. 

The numerically named measurement files are produced everytime you do a
calculation or a simulation. The `000001.xml` file has three parts. The `info`
section includes things like date and time produced, program version number,
etc. The `parameters` section includes the parameters used, and the `results`
section includes some numerical results. For an exact diagonalization problem
this might include the ground state energy, etc. Depending on the simulation
there might be more sections. For example, for a Monte Carlo program there might
be a section `runs`.

It may be the case that you have very large outputs, say the ground state
wavefunction of an exact diagonlization. In this case you can save a binary file
`000001.bin`---or any other filename, but you probably want the filename to
contain the `measurement_id`. If you need to acess this information in the
future the associated `000001.xml` file should have enough information so you
can open it with the same program that produced it and manipulate it again. 

Let us emphasize that, besides establishing a conventional standard data,
directory and file layout, the main underlying idea of COMA is to only have one
single experiments directory. All numerical experiments, from different
projects, with different simulation programs and different parameters, are
stored therein in a simple sequential manner. This greatly simplifies data file
storage. A set of (powerful) tools can then be used to find and retrieve data
and results, based on the stored meta data like date, program, program version,
parameters and so on. This should be vastly easier than trying to remember where
you put that particular raw data file and which undocumented format you used to
store the data two years ago.

Of course, if you don't like the idea of one central data directory, nobody will
stop you from using COMA with multiple directories. Similarly, there might be
use cases where having a file for each measurement is impractical. The data
layout can be adjusted, where the measurements might be stored directly inside
the `experiment.readme.xml` file. Lastly, at least at the moment we are using
XML for all plain text files. Other formats work as well (e.g. JSON), but are
not supported yet. It only has to be some standard format and follow the
conventions outlined above. COMA really is about establishing standards or
conventions, while at the same time allowing flexibility. 

The example experiments directory in `examples/experiments` demonstrates how all
of the above looks in practice. Two example experiments are included, one for
the minimal Python code and one for the minimal C++ code. The README in that
directory details how the experiments where created.

## COMA command line tool

### Installation

COMA requires Python 2.7. Older or newer versions won't work. To install the
Python module and the command line script, go to the COMA source directory and
run the setup.py file with: 
    
    $ ./setup.py install --user

For the time being, we recommend to install to your home directory, and that's
what the `--user` switch does. The install script will give instructions on how
to modify your `.bashrc`, in case the directory where the script got installed
is not in your path.

### Creating new experiments

If everything got installed correctly, you can run the coma command line tool
with, for example,

    $ coma --help

which gives a semi useful help page. The first time coma is run it will create a
configuration file in `~/.config/coma/preferences.conf`. The configuration file
let's you change the location of your data / experiments directory. (It can also
be changed on the command line, via the `--directory` switch.) The default is
`~/experiments`.

To create a new experiment, run:

    $ coma create 

This will create a new experiments directory, if it does not already exist, and
create a new sub-directory therein, for your new experiment, for example
`000001`. In short, `coma create` simplifies using the directory and file
structure outlined above. Running `coma create` again will create another
folder, e.g. `000002`---the folder for the experiment with `experiment_id` 2.

The command line tool implements a simple but efficient templating mechanism.
Templates are used to populate the newly created experiment directory, e.g.
`000002`, with something useful. By default, only a skeleton
`experiment.readme.xml` is created, that can then be filled with useful
information about the experiment. Templates are helpful to set up different files
and scripts (e.g. Bash or Python scripts) for different kinds of experiments or
different projects.

Templates reside in the `templates` directory within the experiments directory
(`~/experiments` by default). The default template is in `templates/default`.
When a new experiment is created all files from the template directory are
copied to the new experiment directory. For files ending in `.template`,
variables are filled in, before they are copied (to the destination file without
the trailing `.template` in the name). Right now, the only supported variable is
`${experiment_id}`. New templates are created simply by creating new directories
in `templates`. When creating a new experiment, the template can be specified on
the command line. For example, to create a new experiment from the template
"python", run:

    $ coma create -t python

The description of a new experiment can also be specified on the command line.
By default, this is written to the `info` section of the `experiment.readme.xml`
file. For example:

    $ coma create -m "My fancy experiment"

### Retrieving data

The coma command line tool comes with a simple mechanism to browse through
experiments and measurements and retrieve data. At this point in time, this is
mostly a demonstration of the COMA project's overarching idea: That it is
possibly to write useful, general tools, as soon as we adhere to conventions for
directory, file and data layout. For an experiments directory with a couple of
experiments and measurements, the following commands should give an idea of what
the coma command line tool can do:

    # Coma retrieve takes one single argument: the path.
    $ coma retrieve '/'
    # Prints all experiments:
    $ coma retrieve '/*'
    # Print all experiment infos
    $ coma retrieve '/*/info' 
    # Print all measurements
    $ coma retrieve '/*/measurements/*'
    # Results of experiment 3
    $ coma retrieve '/3/measurements/*/results'
    # The specific result 'average' for all experiments
    $ coma retrieve '/*/measurements/*/results/average'

### Reset

An experiment can be "reset". This is useful to overwrite existing measurements
in the experiment and, consequently, should be used carefully. A reset simply
sets the measurement index (in `measurements.index.xml`) back to zero.
Subsequent runs of the simulation will then overwrite existing measurement and
data files (e.g. `0000001.xml`, `000002.xml`, and so on). Example:

    # Reset experiment 3
    $ coma reset 3

## C++ implementation

The boost serialization libraries have been used to produce xml files, and also
to save and load large data structures whole. Getting boost to work with your
c++ code can be challenging but hopefully the examples make this clearer and
once it is running it's quite easy to work with. See
examples/minimalC++/README.md for more information.

## TODO

* Do not use `__getstate__` and `__setstate__` for our own serialization,
  because it interferes with Python's pickle protocol (often, but, crucially,
  not always, the goals of our serialization and pickling are more or less the
  same); make methods used configurable
* in experiment: when traversing measurements, do not assume that all
  measurements exist in ascending order --- this is not the case, for example,
  when doing measurements in parallel
* think about how best to handle aborted experiments
* experiment.reset(): delete old measurements (the `*.xml` files)
* parallel / extended experiment class
